#!/bin/bash  

# 检查是否以root权限运行  
if [ "$EUID" -ne 0 ]; then  
  echo "该脚本需要root权限才能运行"  
  # 尝试使用sudo重新运行脚本  
  sudo "bash" "$0" "$@"  
  # 由于sudo会重新运行脚本，所以这里的exit不会被执行到  
  exit 1  
fi  

  
# 定义源脚本的路径（位于当前目录下的 llc 文件夹中）  
SOURCE_SCRIPT="./llc/llc.sh"  
  
# 目标路径  
TARGET_DIR="/usr/local/lib"  
# 软链接路径  
LINK_DIR="/usr/local/bin"  
# 软链接名称  
LINK_NAME="llc"  
  
# 检查源脚本是否存在  
if [ ! -f "$SOURCE_SCRIPT" ]; then  
    echo "源脚本 $SOURCE_SCRIPT 不存在!"  
    exit 1  
fi  
  
# 将脚本移动到目标目录并重命名（如果需要的话）  
cp "$SOURCE_SCRIPT" "$TARGET_DIR/llc.sh"  
  
# 赋予脚本执行权限  
chmod +x "$TARGET_DIR/llc.sh"  
  
# 在 /usr/local/bin 下创建软链接  
ln -s "$TARGET_DIR/llc.sh" "$LINK_DIR/$LINK_NAME"  
  
# 显示成功信息  
echo "脚本已成功安装到 $TARGET_DIR 并在 $LINK_DIR 创建了软链接 $LINK_NAME。"