#!/bin/bash  
  
# 显示扩展后的帮助信息  
function extended_help() {  
    echo "llc 是ll-cli的简化和扩展命令，仍然支持ll-cli原有命令，以下为基本信息:"  
    echo "  -r, run        运行应用或服务 (Run an application or service)"  
    echo "  -p, ps         显示进程或服务状态 (Show process or service status)"  
    echo "  -e, exec       执行命令 (Execute a command)"  
    echo "  -k, kill       结束进程或服务 (Kill a process or service)"  
    echo "  -i, install    安装应用、包或服务 (Install an application, package, or service)"  
    echo "  -u, uninstall  卸载应用、包或服务 (Uninstall an application, package, or service)"  
    echo "  -up, update     更新应用、包或服务 (Update an application, package, or service)"  
    echo "  -q, query      查询应用、包或服务的状态或信息 (Query the status or information of an application, package, or service)"  
    echo "  -l, list       列出所有的应用、包或服务 (List all applications, packages, or services)"  
    echo "  -rp, repo       管理软件包仓库 (Manage software package repositories)"  
    echo "  -h, help       显示帮助信息 (Display help information)"  
    echo "原有的 ll-cli 命令选项和子命令仍然有效。"  
    echo "Original ll-cli command options and subcommands are still valid."  
}  
  
# 解析参数并执行相应操作  
while [[ $# -gt 0 ]]; do  
    case $1 in  
        -h|help)  
            extended_help  
            exit 0  
            ;;  
        -r|run)  
            shift  
            command ll-cli run "$@"  
            exit $?  
            ;;  
        -p|ps)  
            shift  
            command ll-cli ps "$@"  
            exit $?  
            ;;  
        -e|exec)  
            shift  
            command ll-cli exec "$@"  
            exit $?  
            ;;  
        -k|kill)  
            shift  
            command ll-cli kill "$@"  
            exit $?  
            ;;  
        -i|install)  
            shift  
            command ll-cli install "$@"  
            exit $?  
            ;;  
        -u|uninstall)  
            shift  
            command ll-cli uninstall "$@"  
            exit $?  
            ;;  
        -up|update)  
            shift  
            command ll-cli update "$@"  
            exit $?  
            ;;  
        -q|query)  
            shift  
            command ll-cli query "$@"  
            exit $?  
            ;;  
        -l|list)  
            shift  
            command ll-cli list "$@"  
            exit $?  
            ;;  
        -rp|repo)  
            shift  
            command ll-cli repo "$@"  
            exit $?  
            ;;  
        *)  
            # 对于其他未处理的参数，检查是否为原始的 ll-cli 子命令  
            if command -v "ll-cli-$1" >/dev/null 2>&1; then  
                command ll-cli "$1" "${@:2}"  
                exit $?  
            else  
                echo "未知命令: $1"  
                extended_help  
                exit 1  
            fi  
            ;;  
    esac  
    shift # 处理下一个参数  
done  
  
# 如果没有提供任何参数，显示扩展帮助信息  
if [ $# -eq 0 ]; then  
    extended_help  
    exit 0  
fi