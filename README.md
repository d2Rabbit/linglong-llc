# llc

#### 介绍
玲珑包ll-cli命令简化和扩展脚本,旨在简化ll-cli这个命令


#### 使用说明

1.  直接使用sudo或者直接执行install.sh脚本，会直接将llc脚本移动到对应位置
2.  安装后，可以使用llc命令代替ll-cli命令，具体参考如下图
    ![输入图片说明](https://foruda.gitee.com/images/1705042621658981927/1c65d6e7_418674.png "屏幕截图")
